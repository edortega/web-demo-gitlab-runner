![](https://forge.etsi.org/gitlab/help/ci/img/cicd_pipeline_infograph.png)
# Proof of Concept for Gitlab-runner on Docker in Docket build-in or Gitlab-runner as binary

This project contain:
```
.
├── Dockerfile
├── .gitlab-ci.yml
├── README.md
└── src
    ├── web.go
    └── web_test.go
```

At the end of this exercise you will get the service posted on [http://localhost/](http://localhost). You can also append a endpoint like **your name** and get a messages, example: [http://localhost/John Doe](http://localhost/John%20Doe)

## Gitlab-runner as binary

[https://docs.gitlab.com/runner/install/linux-manually.html](https://docs.gitlab.com/runner/install/linux-manually.html)

#### Download gitlab-runner for repo
```
wget  https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
```

#### Change permission
```
sudo chmod +x /usr/local/bin/gitlab-runner
```

#### Create User
```
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash 
```

#### Allow User Docker permission
```
usermod -aG docker gitlab-runner
```
#### Installing gitlab-runner

```

sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

```
That will register the service on **systemcl**, so in the future you can ask for **status**  and other important command

#### Register gitlab-runne 
```
sudo gitlab-runner register
```

After finish you registration, you must modified the **conf.toml** file, with the info as follow:

```
  name = "my-runner-local"
  url = "https://gitlab.com"
  token = "YOU_TOCKEN_ID"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:stable-dind"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]

``` 
Now you must **restart** the service, and that will be all. You will have a **gitlab-runner** woriking 

``` 
 systemctl restart gitlab-runner
```

If the service is runnig, you can check as was mentioned before:
```
● gitlab-runner.service - GitLab Runner
   Loaded: loaded (/etc/systemd/system/gitlab-runner.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2019-05-13 13:30:16 UTC; 1h 3min ago
 Main PID: 540 (gitlab-runner)
    Tasks: 8
   Memory: 28.7M
      CPU: 1.460s
   CGroup: /system.slice/gitlab-runner.service
           └─540 /usr/local/bin/gitlab-runner run --working-directory /home/gitlab-runner --config /etc/gitlab-runner/config.toml --service gitlab-runner --syslog

May 13 13:30:16 docker gitlab-runner[540]: Running in system-mode.
May 13 13:30:16 docker gitlab-runner[540]: Running in system-mode.
May 13 13:30:16 docker gitlab-runner[540]:

```

## Docker in Docker build-in


#### First step: Setting up the server

Configurating docker for **overlay2** support:

#### Checking if the host has **overlay** module loaded:
``` 
modprobe overlay 
```

#### Configure Docker fot suppor **overlay2**
```
echo " {
  "storage-driver": "overlay2"
} " >> /etc/docker/daemon.json
```

And restart the service

```
service docker restart 
```

#### Second step: Create Docker in Docker

#### Create docker **gitlab-runner** network

```
docker network create gitlab-runner-net 
```

#### Create docker in docker container
``` 
docker run -d \
  --name gitlab-dind \
  --privileged \
  --restart always \
  --network gitlab-runner-net \
  -v /var/lib/docker \
   docker:17.06.0-ce-dind \
  --storage-driver=overlay2
```
