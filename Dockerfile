FROM golang:1.12.4-alpine3.9 AS web

LABEL maintainer="Edward Ortega <edward.josette@gmail.com>", \
      description="web"
      

RUN apk --update add git less && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

WORKDIR /src

COPY . .

RUN go build -o web src/web.go

FROM alpine:3.7

LABEL maintainer="Edward Ortega <edward.josette@gmail.com>", \
      description="web"

COPY --from=web  /src/web /bin/web

RUN chmod +x /bin/web

EXPOSE 80

ENTRYPOINT ["web"]
